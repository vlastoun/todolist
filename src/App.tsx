import React from "react";
import { Switch, Route } from "react-router-dom";
import TodoList from "./components/TodoList";
import { TodoListContext } from "./components/TodolistProvider";
import TodoDetail from "./components/TodoDetail";
import { useGetTodos } from "./components/Api";
import { CenterWrapper } from "./components/styled";
import CircularProgress from "@material-ui/core/CircularProgress";

function App() {
  // I would like to get todos like this
  const { loading, todos } = useGetTodos(
    "https://5f4ff0fa2b5a260016e8b2f6.mockapi.io/api/v1/todos"
  );

  return loading ? (
    <CenterWrapper>
      <CircularProgress />
    </CenterWrapper>
  ) : (
    <TodoListContext.Provider value={{ todos: todos || [] }}>
      <Switch>
        <Route exact path="/">
          <TodoList />
        </Route>
        <Route path="/detail/:id">
          <TodoDetail />
        </Route>
      </Switch>
    </TodoListContext.Provider>
  );
}

export default App;
