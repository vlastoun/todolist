import React, { useContext } from "react";
import { useParams } from "react-router-dom";
import { TodoListContext } from "./TodolistProvider";
import Grid from "@material-ui/core/Grid";
import { Card } from "@material-ui/core";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const TodoDetail: React.FC = () => {
  const { id } = useParams<{ id: string }>();
  const { todos } = useContext(TodoListContext);

  const selectedTodo = todos.find((todo) => todo.id === id);

  return (
    <Grid container justify="center">
      <Grid xs={3} item>
        <Card>
          <CardContent>
            {selectedTodo ? (
              <>
                <Typography>Title: {selectedTodo.title}</Typography>
                <Typography>id: {selectedTodo.id}</Typography>
              </>
            ) : (
              <Typography>No todo found</Typography>
            )}
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

export default TodoDetail;
