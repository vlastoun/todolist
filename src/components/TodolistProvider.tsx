import React from "react";
import { TodoEntity } from "./types";

interface ITodoListContext {
  todos: TodoEntity[];
}

export const TodoListContext = React.createContext<ITodoListContext>({
  todos: [],
});
