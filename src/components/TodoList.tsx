import React, { useContext } from "react";
import Todo from "./Todo";
import { TodoListContext } from "./TodolistProvider";
import { List, Card } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import CardContent from "@material-ui/core/CardContent";

const TodoList: React.FC = () => {
  const { todos } = useContext(TodoListContext);
  return (
    <Grid container justify="center">
      <Grid xs={3} item>
        <Card>
          <CardContent>
            <Typography variant="h3">ToDo list</Typography>
            <List component="nav">
              {todos.map((todo) => (
                <Todo todo={todo} key={todo.id} />
              ))}
            </List>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

export default TodoList;
