export type TodoEntity = {
  id: string;
  title: string;
};
