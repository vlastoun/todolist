import React from "react";
import { Link } from "react-router-dom";
import { ListItem } from "@material-ui/core";
import ListItemText from "@material-ui/core/ListItemText";

interface TodoProps {
  todo: {
    id: string;
    title: string;
  };
}

const Todo: React.FC<TodoProps> = ({ todo: { id, title } }) => {
  return (
    <ListItem component={Link} to={`/detail/${id}`} button>
      <ListItemText>{title}</ListItemText>
    </ListItem>
  );
};

// I am not used to write class components... I had some issue with
// shouldComponentUpdate and it behaved in strange way..
// I rewrote it to FC this should prevent of rendering, only when props change
export default React.memo(Todo);
