import styled from "@material-ui/core/styles/styled";

export const CenterWrapper = styled("div")({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  width: "100%",
  height: "100%",
  minHeight: "100vh"
});
