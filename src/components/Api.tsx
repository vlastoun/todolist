import React, { useState } from "react";
import { TodoEntity } from "./types";
import axios from "axios";

export const useGetTodos = (url: string) => {
  const [todos, setTodos] = useState<TodoEntity[]>();
  const [loading, setLoading] = useState<boolean>();

  React.useEffect(() => {
    (async () => {
      setLoading(true);
      const { data: todos } = await axios.get<TodoEntity[]>(url);
      // Removed for loop and prevent rendering with every item
      setTodos(todos);
      setLoading(false);
    })();
  }, [url]);

  return { loading, todos };
};
